class FeaturedImage {
  String thumbnail;
  String mediam;
  String large;

  FeaturedImage({this.thumbnail, this.mediam, this.large});

  FeaturedImage.fromJson(Map<String, dynamic> json) {
    thumbnail = json['thumbnail'];
    mediam = json['mediam'];
    large = json['large'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['thumbnail'] = this.thumbnail;
    data['mediam'] = this.mediam;
    data['large'] = this.large;
    return data;
  }
}
