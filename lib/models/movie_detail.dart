class MovieDetail {
  int id;
  String title;
  String slug;
  String date;
  String content;
  List<Categories> categories;
  List<Casts> casts;
  String youtubeId;
  String imdbRating;
  String imdbVotes;
  String originalTitle;
  String releaseDate;
  String voteAverage;
  String voteCount;
  String tagline;
  String runtime;
  List<String> galleryImages;
  FeaturedImage featuredImage;

  MovieDetail(
      {this.id,
      this.title,
      this.slug,
      this.date,
      this.content,
      this.categories,
      this.casts,
      this.youtubeId,
      this.imdbRating,
      this.imdbVotes,
      this.originalTitle,
      this.releaseDate,
      this.voteAverage,
      this.voteCount,
      this.tagline,
      this.runtime,
      this.galleryImages,
      this.featuredImage});

  MovieDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    slug = json['slug'];
    date = json['date'];
    content = json['content'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    if (json['casts'] != null) {
      casts = new List<Casts>();
      json['casts'].forEach((v) {
        casts.add(new Casts.fromJson(v));
      });
    }
    youtubeId = json['youtube_id'];
    imdbRating = json['imdbRating'];
    imdbVotes = json['imdbVotes'];
    originalTitle = json['original_title'];
    releaseDate = json['release_date'];
    voteAverage = json['vote_average'];
    voteCount = json['vote_count'];
    tagline = json['tagline'];
    runtime = json['runtime'];
    galleryImages = json['gallery_images'].cast<String>();
    featuredImage = json['featured_image'] != null
        ? new FeaturedImage.fromJson(json['featured_image'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['slug'] = this.slug;
    data['date'] = this.date;
    data['content'] = this.content;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    if (this.casts != null) {
      data['casts'] = this.casts.map((v) => v.toJson()).toList();
    }
    data['youtube_id'] = this.youtubeId;
    data['imdbRating'] = this.imdbRating;
    data['imdbVotes'] = this.imdbVotes;
    data['original_title'] = this.originalTitle;
    data['release_date'] = this.releaseDate;
    data['vote_average'] = this.voteAverage;
    data['vote_count'] = this.voteCount;
    data['tagline'] = this.tagline;
    data['runtime'] = this.runtime;
    data['gallery_images'] = this.galleryImages;
    if (this.featuredImage != null) {
      data['featured_image'] = this.featuredImage.toJson();
    }
    return data;
  }
}

class Categories {
  int termId;
  String name;

  Categories({
    this.termId,
    this.name,
  });

  Categories.fromJson(Map<String, dynamic> json) {
    termId = json['term_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['term_id'] = this.termId;
    data['name'] = this.name;
    return data;
  }
}

class Casts {
  String image;
  String name;

  Casts({this.image, this.name});

  Casts.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['name'] = this.name;
    return data;
  }
}

class FeaturedImage {
  String thumbnail;
  String mediam;
  String large;

  FeaturedImage({this.thumbnail, this.mediam, this.large});

  FeaturedImage.fromJson(Map<String, dynamic> json) {
    thumbnail = json['thumbnail'];
    mediam = json['mediam'];
    large = json['large'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['thumbnail'] = this.thumbnail;
    data['mediam'] = this.mediam;
    data['large'] = this.large;
    return data;
  }
}
