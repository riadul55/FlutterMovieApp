import 'featured_image.dart';

class Movie {
  int id;
  String title;
  String content;
  String date;
  String slug;
  String ratings;
  String ids;
  String type;
  String youtubeId;
  FeaturedImage featuredImage;

  Movie(
      {this.id,
        this.title,
        this.content,
        this.date,
        this.slug,
        this.ratings,
        this.ids,
        this.type,
        this.youtubeId,
        this.featuredImage});

  Movie.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    content = json['content'];
    date = json['date'];
    slug = json['slug'];
    ratings = json['ratings'];
    ids = json['ids'];
    type = json['type'];
    youtubeId = json['youtube_id'];
    featuredImage = json['featured_image'] != null
        ? new FeaturedImage.fromJson(json['featured_image'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['content'] = this.content;
    data['date'] = this.date;
    data['slug'] = this.slug;
    data['ratings'] = this.ratings;
    data['ids'] = this.ids;
    data['type'] = this.type;
    data['youtube_id'] = this.youtubeId;
    if (this.featuredImage != null) {
      data['featured_image'] = this.featuredImage.toJson();
    }
    return data;
  }
}