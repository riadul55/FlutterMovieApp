import 'package:movie_app/models/movie_detail.dart';

class InfoResponse {
  final Info info;
  final String error;

  InfoResponse({this.info, this.error});

  InfoResponse.fromJson(Map<String, dynamic> json)
      : info = new Info.fromJson(json),
        error = "";

  InfoResponse.withError(String errorValue)
      : info = Info(),
        error = errorValue;
}

class Info {
  List<Categories> categories;
  String imdbRating;
  String imdbVotes;
  String originalTitle;
  String releaseDate;

  Info(
      {this.categories,
      this.imdbRating,
      this.imdbVotes,
      this.originalTitle,
      this.releaseDate});

  Info.fromJson(Map<String, dynamic> json) {
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    imdbRating = json['imdbRating'];
    imdbVotes = json['imdbVotes'];
    originalTitle = json['original_title'];
    releaseDate = json['release_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['imdbRating'] = this.imdbRating;
    data['imdbVotes'] = this.imdbVotes;
    data['original_title'] = this.originalTitle;
    data['release_date'] = this.releaseDate;
    return data;
  }
}
