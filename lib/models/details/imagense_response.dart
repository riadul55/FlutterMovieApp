class ImagenseResponse {
  List<String> galleryImages;
  String error;

  ImagenseResponse({this.galleryImages, this.error});

  ImagenseResponse.fromJson(List<dynamic> json)
      : galleryImages = json.cast<String>(),
        error = "";

  ImagenseResponse.withError(String errorValue)
      : galleryImages = List(),
        error = errorValue;
}
