class EpisodeResponse {
  List<Episode> episodes;
  String errors;

  EpisodeResponse({this.episodes, this.errors});

  EpisodeResponse.fromJson(List<dynamic> json)
      : episodes = (json).map((i) => new Episode.fromJson(i)).toList(),
        errors = "";

  EpisodeResponse.withError(String errorValue)
      : episodes = List(),
        errors = errorValue;
}

class Episode {
  int id;
  String date;
  String title;
  String type;
  Metas metas;

  Episode({this.id, this.date, this.title, this.type, this.metas});

  Episode.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    date = json['date'];
    title = json['title'];
    type = json['type'];
    metas = json['metas'] != null ? new Metas.fromJson(json['metas']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['date'] = this.date;
    data['title'] = this.title;
    data['type'] = this.type;
    if (this.metas != null) {
      data['metas'] = this.metas.toJson();
    }
    return data;
  }
}

class Metas {
  String ids;
  String temporada;
  String episodio;
  String episodeName;
  String serie;
  String dtBackdrop;
  String imagenes;
  String airDate;
  List<Players> players;

  Metas(
      {this.ids,
      this.temporada,
      this.episodio,
      this.episodeName,
      this.serie,
      this.dtBackdrop,
      this.imagenes,
      this.airDate,
      this.players});

  Metas.fromJson(Map<String, dynamic> json) {
    ids = json['ids'];
    temporada = json['temporada'];
    episodio = json['episodio'];
    episodeName = json['episode_name'];
    serie = json['serie'];
    dtBackdrop = json['dt_backdrop'];
    imagenes = json['imagenes'];
    airDate = json['air_date'];
    if (json['players'] != null) {
      players = new List<Players>();
      json['players'].forEach((v) {
        players.add(new Players.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ids'] = this.ids;
    data['temporada'] = this.temporada;
    data['episodio'] = this.episodio;
    data['episode_name'] = this.episodeName;
    data['serie'] = this.serie;
    data['dt_backdrop'] = this.dtBackdrop;
    data['imagenes'] = this.imagenes;
    data['air_date'] = this.airDate;
    if (this.players != null) {
      data['players'] = this.players.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Players {
  String name;
  String select;
  String idioma;
  String url;

  Players({this.name, this.select, this.idioma, this.url});

  Players.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    select = json['select'];
    idioma = json['idioma'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['select'] = this.select;
    data['idioma'] = this.idioma;
    data['url'] = this.url;
    return data;
  }
}
