class StreamResponse {
  final Streams streams;
  final String error;

  StreamResponse({this.streams, this.error});

  StreamResponse.fromJson(Map<String, dynamic> json)
      : streams = new Streams.fromJson(json),
        error = "";

  StreamResponse.withError(String errorValue)
      : streams = Streams(),
        error = errorValue;
}

class Streams {
  List<Movies> movies;

  Streams({this.movies});

  Streams.fromJson(Map<String, dynamic> json) {
    if (json['movies'] != null) {
      movies = new List<Movies>();
      json['movies'].forEach((v) {
        movies.add(new Movies.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.movies != null) {
      data['movies'] = this.movies.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Movies {
  String name;
  String select;
  String idioma;
  String url;

  Movies({this.name, this.select, this.idioma, this.url});

  Movies.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    select = json['select'];
    idioma = json['idioma'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['select'] = this.select;
    data['idioma'] = this.idioma;
    data['url'] = this.url;
    return data;
  }
}