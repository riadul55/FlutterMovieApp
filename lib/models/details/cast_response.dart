class CastResponse {
  List<Cast> casts;
  String error;

  CastResponse({this.casts, this.error});

  CastResponse.fromJson(List<dynamic> json)
      : casts = (json).map((i) => new Cast.fromJson(i)).toList(),
        error = "";

  CastResponse.withError(String errorValue)
      : casts = List(),
        error = errorValue;
}

class Cast {
  String image;
  String name;

  Cast({this.image, this.name});

  Cast.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['name'] = this.name;
    return data;
  }
}
