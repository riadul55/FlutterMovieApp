class SeasonResponse {
  List<Season> seasons;
  String errors;

  SeasonResponse({this.seasons, this.errors});

  SeasonResponse.fromJson(List<dynamic> json)
      : seasons = (json).map((i) => new Season.fromJson(i)).toList(),
        errors = "";

  SeasonResponse.withError(String errorValue)
      : seasons = List(),
        errors = errorValue;
}

class Season {
  int id;
  String date;
  String title;
  String type;
  Metas metas;

  Season({this.id, this.date, this.title, this.type, this.metas});

  Season.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    date = json['date'];
    title = json['title'];
    type = json['type'];
    metas = json['metas'] != null ? new Metas.fromJson(json['metas']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['date'] = this.date;
    data['title'] = this.title;
    data['type'] = this.type;
    if (this.metas != null) {
      data['metas'] = this.metas.toJson();
    }
    return data;
  }
}

class Metas {
  String ids;
  String temporada;
  String clgnrt;
  String serie;
  String airDate;

  Metas({this.ids, this.temporada, this.clgnrt, this.serie, this.airDate});

  Metas.fromJson(Map<String, dynamic> json) {
    ids = json['ids'];
    temporada = json['temporada'];
    clgnrt = json['clgnrt'];
    serie = json['serie'];
    airDate = json['air_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ids'] = this.ids;
    data['temporada'] = this.temporada;
    data['clgnrt'] = this.clgnrt;
    data['serie'] = this.serie;
    data['air_date'] = this.airDate;
    return data;
  }
}
