import 'package:movie_app/models/genre.dart';

class GenreResponse {
  final List<Genre> genres;
  final String errors;

  GenreResponse(this.genres, this.errors);

  GenreResponse.fromJson(List<dynamic> json)
      : genres = (json).map((i) => new Genre.fromJson(i)).toList(),
        errors = "";

  GenreResponse.withError(String errorValue)
      : genres = List(),
        errors = errorValue;
}
