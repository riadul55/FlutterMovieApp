import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'main_event.dart';
part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  MainBloc() : super(MainInitial());

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {
    if (event is TopRatedEvent) {
      yield TopRatedState();
    } else if (event is RecentEvent) {
      yield RecentState();
    } else if (event is DiscoverEvent) {
      yield DiscoverState();
    } else if (event is ProfileEvent) {
      yield ProfileState();
    } else if (event is SettingsEvent) {
      yield SettingsState();
    } else if (event is AboutEvent) {
      yield AboutState();
    } else if (event is PopEvent) {
      yield PopState();
    } else {
      yield MainInitial();
    }
  }
}
