part of 'main_bloc.dart';

@immutable
abstract class MainEvent {}

class TopRatedEvent extends MainEvent {}

class RecentEvent extends MainEvent {}

class DiscoverEvent extends MainEvent {}

class ProfileEvent extends MainEvent {}

class SettingsEvent extends MainEvent {}

class AboutEvent extends MainEvent {}

class PopEvent extends MainEvent {}
