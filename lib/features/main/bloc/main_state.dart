part of 'main_bloc.dart';

@immutable
abstract class MainState {}

class MainInitial extends MainState {}

class TopRatedState extends MainState {}

class RecentState extends MainState {}

class DiscoverState extends MainState {}

class ProfileState extends MainState {}

class SettingsState extends MainState {}

class AboutState extends MainState {}

class PopState extends MainState {}
