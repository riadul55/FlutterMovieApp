import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/features/auth/welcome.dart';
import 'package:movie_app/features/home/home.dart';
import 'package:movie_app/features/recents/recents.dart';
import 'package:movie_app/features/search/search.dart';
import 'package:movie_app/features/topRateds/top_rated.dart';

import 'bloc/main_bloc.dart';
import 'package:movie_app/styles/theme.dart' as Style;
import 'package:toast/toast.dart';

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MainBloc(),
      child:
          BlocBuilder<MainBloc, MainState>(builder: (blocContext, blocState) {
        return WillPopScope(
            child: Scaffold(
              key: _scaffoldKey,
              backgroundColor: Style.Colors.mainColor,
              appBar: blocState is DiscoverState
                  ? null
                  : AppBar(
                      backgroundColor: Style.Colors.mainColor,
                      centerTitle: true,
                      leading: IconButton(
                        icon: Icon(
                          EvaIcons.menu2Outline,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          _scaffoldKey.currentState.openDrawer();
                        },
                      ),
                      title: Text(getTitle(blocState)),
                      actions: <Widget>[
                        IconButton(
                            icon: Icon(
                              EvaIcons.searchOutline,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Search(),
                                  ));
                            })
                      ],
                    ),
              drawer: Drawer(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    UserAccountsDrawerHeader(
                      decoration: BoxDecoration(
                        color: Style.Colors.mainColor,
                      ),
                      accountName: Text(
                        'John Doe',
                        style: TextStyle(color: Colors.white),
                      ),
                      accountEmail: Text(
                        'example@gmail.com',
                        style: TextStyle(color: Colors.white),
                      ),
                      currentAccountPicture: CircleAvatar(
                        backgroundColor: Style.Colors.titleColor,
                        child: Icon(
                          Icons.person,
                          size: 50,
                        ),
                      ),
                    ),
                    ListTile(
                      leading: Icon(EvaIcons.homeOutline),
                      title: Text('Home'),
                      onTap: () {
                        _scaffoldKey.currentState.openEndDrawer();
                        BlocProvider.of<MainBloc>(blocContext).add(PopEvent());
                      },
                    ),
                    ListTile(
                      leading: Icon(EvaIcons.stopCircle),
                      title: Text('Top Rated'),
                      onTap: () {
                        _scaffoldKey.currentState.openEndDrawer();
                        BlocProvider.of<MainBloc>(blocContext)
                            .add(TopRatedEvent());
                      },
                    ),
                    ListTile(
                      leading: Icon(EvaIcons.stopCircle),
                      title: Text('Recent'),
                      onTap: () {
                        _scaffoldKey.currentState.openEndDrawer();
                        BlocProvider.of<MainBloc>(blocContext)
                            .add(RecentEvent());
                      },
                    ),
                    ListTile(
                      leading: Icon(EvaIcons.searchOutline),
                      title: Text('Discover'),
                      onTap: () {
                        _scaffoldKey.currentState.openEndDrawer();
                        BlocProvider.of<MainBloc>(blocContext)
                            .add(DiscoverEvent());
                      },
                    ),
                    Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Style.Colors.secondColor,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ListTile(
                      leading: Icon(EvaIcons.personOutline),
                      title: Text('Profile'),
                      onTap: () {
                        _scaffoldKey.currentState.openEndDrawer();
                        BlocProvider.of<MainBloc>(blocContext)
                            .add(ProfileEvent());
                      },
                    ),
                    ListTile(
                      leading: Icon(EvaIcons.settingsOutline),
                      title: Text('Settings'),
                      onTap: () {
                        _scaffoldKey.currentState.openEndDrawer();
                        BlocProvider.of<MainBloc>(blocContext)
                            .add(SettingsEvent());
                      },
                    ),
                    ListTile(
                      leading: Icon(EvaIcons.infoOutline),
                      title: Text('About'),
                      onTap: () {
                        _scaffoldKey.currentState.openEndDrawer();
                        BlocProvider.of<MainBloc>(blocContext)
                            .add(AboutEvent());
                      },
                    ),
                    Container(
                      height: 1.0,
                      width: MediaQuery.of(context).size.width,
                      color: Style.Colors.secondColor,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ListTile(
                      leading: Icon(EvaIcons.logOutOutline),
                      title: Text('Log Out'),
                      onTap: () {
                        _scaffoldKey.currentState.openEndDrawer();

                        Navigator.pushReplacement(
                            blocContext,
                            MaterialPageRoute(
                              builder: (context) => Welcome(),
                            ));
                      },
                    ),
                  ],
                ),
              ),
              body: getBody(blocState, context: blocContext),
            ),
            onWillPop: _onBackPressed);
      }),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Are you sure?'),
              content: Text('Do you want to exit App?'),
              actions: <Widget>[
                FlatButton(
                  child: Text('No'),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                FlatButton(
                  child: Text('Yes'),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                )
              ],
            );
          },
        ) ??
        false;
  }

  Widget getBody(blocState, {context}) {
    if (blocState is TopRatedState) {
      return TopRateds(
        context: context,
      );
    } else if (blocState is RecentState) {
      return Recents(
        context: context,
      );
    } else if (blocState is DiscoverState) {
      return Search(
        context: context,
      );
    } else if (blocState is ProfileState) {
      return Container();
    } else if (blocState is SettingsState) {
      return Container();
    } else if (blocState is AboutState) {
      return Container();
    } else if (blocState is PopState) {
      return Home();
    } else {
      return Home();
    }
  }

  String getTitle(blocState) {
    if (blocState is TopRatedState) {
      return "Top Rated";
    } else if (blocState is RecentState) {
      return "Recent";
    } else if (blocState is ProfileState) {
      return "Profile";
    } else if (blocState is SettingsState) {
      return "Settings";
    } else if (blocState is AboutState) {
      return "About";
    } else if (blocState is DiscoverState) {
      return "Search";
    } else {
      return "Movie App";
    }
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }
}
