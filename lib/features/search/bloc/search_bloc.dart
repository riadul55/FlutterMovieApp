import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:movie_app/models/movie_response.dart';
import 'package:movie_app/repository/movie_repository.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchBloc() : super(SearchInitial());

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is SearchRequest) {
      MovieResponse response = await new MovieRepository()
          .getMoviesByQuery(event.value); // value will be passed for query
      yield SearchData(response);
    }
  }
}
