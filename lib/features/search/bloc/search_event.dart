part of 'search_bloc.dart';

@immutable
abstract class SearchEvent {}

class SearchRequest extends SearchEvent {
  final String value;

  SearchRequest(this.value);
}
