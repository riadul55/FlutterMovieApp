part of 'search_bloc.dart';

@immutable
abstract class SearchState {}

class SearchInitial extends SearchState {}

class SearchData extends SearchState {
  final MovieResponse response;

  SearchData(this.response);
}
