import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movie_app/features/details/details.dart';
import 'package:movie_app/features/main/bloc/main_bloc.dart';
import 'package:movie_app/features/search/bloc/search_bloc.dart';
import 'package:movie_app/models/movie_response.dart';
import 'package:movie_app/styles/theme.dart' as Style;

class Search extends StatefulWidget {
  final BuildContext context;

  Search({Key key, this.context}) : super(key: key);

  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SearchBloc(),
      child: BlocBuilder<SearchBloc, SearchState>(
          builder: (blocContext, blocState) {
        BlocProvider.of<SearchBloc>(blocContext).add(SearchRequest(""));
        return WillPopScope(
            child: Scaffold(
                backgroundColor: Style.Colors.mainColor,
                appBar: AppBar(
                  backgroundColor: Style.Colors.mainColor,
                  leading: Builder(
                    builder: (BuildContext context) {
                      return IconButton(
                        icon: Icon(Icons.arrow_back),
                        onPressed: () {
                          if (widget.context != null) {
                            BlocProvider.of<MainBloc>(widget.context).add(PopEvent());
                          } else {
                            Navigator.pop(context);
                          }
                        },
                      );
                    },
                  ),
                  title: Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    decoration: BoxDecoration(
                      color: Style.Colors.mainColor,
                      border: Border.all(
                        color: Style.Colors.titleColor,
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(16.0),
                    ),
                    child: TextField(
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Search here....",
                        hintStyle: TextStyle(color: Style.Colors.titleColor),
                      ),
                      onChanged: (value) {
                        BlocProvider.of<SearchBloc>(blocContext)
                            .add(SearchRequest(value));
                      },
                    ),
                  ),
                ),
                body: blocState is SearchData
                    ? _buildSearchListWidget(blocState.response)
                    : _buildLoadingWidget()),
            onWillPop: () {
              if (widget.context != null) {
                BlocProvider.of<MainBloc>(widget.context).add(PopEvent());
              } else {
                Navigator.pop(context);
              }
              return;
            });
      }),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSearchListWidget(MovieResponse data) {
    if (data.error != null && data.error.length > 0) {
      return _buildLoadingWidget();
    } else if (data != null && data.movies != null && data.movies.length == 0) {
      return _buildErrorWidget("Empty");
    } else
      return ListView.builder(
        itemCount: data.movies.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(
              left: 10,
              right: 10,
              top: 8,
            ),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MovieDetails(
                        movie: data.movies[index],
                      ),
                    ));
              },
              child: Card(
                elevation: 4,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: data.movies[index].featuredImage.thumbnail == null
                          ? Container(
                              width: 50.0,
                              height: 130.0,
                              decoration: BoxDecoration(
                                  color: Style.Colors.secondColor,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2.0)),
                                  shape: BoxShape.rectangle),
                              child: Column(
                                children: <Widget>[
                                  Icon(
                                    EvaIcons.filmOutline,
                                    color: Colors.white,
                                    size: 40.0,
                                  ),
                                ],
                              ),
                            )
                          : Container(
                              width: 50.0,
                              height: 130.0,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(2.0)),
                                shape: BoxShape.rectangle,
                                image: DecorationImage(
                                    image: NetworkImage(data
                                        .movies[index].featuredImage.thumbnail),
                                    fit: BoxFit.cover),
                              ),
                            ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              data.movies[index].title,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  data.movies[index].ratings,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 10.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                RatingBar.builder(
                                  itemSize: 8.0,
                                  initialRating:
                                      double.parse(data.movies[index].ratings) /
                                          2,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: true,
                                  itemCount: 5,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 2.0),
                                  itemBuilder: (context, _) => Icon(
                                    EvaIcons.star,
                                    color: Style.Colors.secondColor,
                                  ),
                                  onRatingUpdate: (rating) {
                                    print(rating);
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "$error",
            style: TextStyle(
              color: Style.Colors.secondColor,
            ),
          ),
        ],
      ),
    );
  }
}
