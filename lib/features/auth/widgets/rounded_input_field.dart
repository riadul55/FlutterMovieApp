import 'package:flutter/material.dart';
import 'package:movie_app/features/auth/widgets/text_field_container.dart';
import 'package:movie_app/styles/theme.dart' as Style;

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const RoundedInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        onChanged: onChanged,
        cursorColor: Style.Colors.secondColor,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: Style.Colors.secondColor,
          ),
          hintText: hintText,
          hintStyle: TextStyle(color: Style.Colors.titleColor),
          border: InputBorder.none,
        ),
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
}
