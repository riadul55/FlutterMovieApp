import 'package:flutter/material.dart';
import 'package:movie_app/features/auth/widgets/text_field_container.dart';
import 'package:movie_app/styles/theme.dart' as Style;

class RoundedPasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const RoundedPasswordField({
    Key key,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChanged,
        cursorColor: Style.Colors.secondColor,
        decoration: InputDecoration(
          hintText: "Password",
          hintStyle: TextStyle(color: Style.Colors.titleColor),
          icon: Icon(
            Icons.lock,
            color: Style.Colors.secondColor,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: Style.Colors.secondColor,
          ),
          border: InputBorder.none,
        ),
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
}
