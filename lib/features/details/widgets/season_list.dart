import 'dart:math';

import 'package:flutter/material.dart';
import 'package:movie_app/bloc/get_episodes_bloc.dart';
import 'package:movie_app/features/details/widgets/seasons_episodes.dart';
import 'package:movie_app/models/details/season_response.dart';
import 'package:movie_app/styles/theme.dart' as Style;

class SeasonList extends StatefulWidget {
  final List<Season> seasons;

  SeasonList({Key key, @required this.seasons}) : super(key: key);

  @override
  _SeasonListState createState() => _SeasonListState(seasons);
}

class _SeasonListState extends State<SeasonList>
    with SingleTickerProviderStateMixin {
  final List<Season> seasons;
  TabController _tabController;

  _SeasonListState(this.seasons);

  int listLength = 1;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: seasons.length, vsync: this);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging) {
        episodesBloc..drainStream();
      }
    });

    print(seasons);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(listLength);
    return Container(
      height: (listLength * 155) + 100.0,
      child: DefaultTabController(
        length: seasons.length,
        child: Scaffold(
          backgroundColor: Style.Colors.mainColor,
          appBar: PreferredSize(
            child: AppBar(
              backgroundColor: Style.Colors.mainColor,
              bottom: TabBar(
                controller: _tabController,
                indicatorColor: Style.Colors.secondColor,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorWeight: 3.0,
                unselectedLabelColor: Style.Colors.titleColor,
                labelColor: Colors.white,
                isScrollable: true,
                tabs: seasons.map((Season season) {
                  return Container(
                    padding: EdgeInsets.only(bottom: 15.0, top: 10.0),
                    child: Text(
                      season.title.split(":")[1].toUpperCase(),
                      style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
            preferredSize: Size.fromHeight(50.0),
          ),
          body: TabBarView(
            controller: _tabController,
            physics: NeverScrollableScrollPhysics(),
            children: seasons.map((Season season) {
              return SeasonsEpisodes(
                ids: season.metas.ids,
                temp: season.metas.temporada != null ? season.metas.temporada : "1",
                callback: (val) => setState(() => listLength = val),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
