import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/bloc/get_movie_streams_bloc.dart';
import 'package:movie_app/features/details/widgets/hls_player.dart';
import 'package:movie_app/features/details/widgets/iframe_player.dart';
import 'package:movie_app/models/details/stream_response.dart';
import 'package:movie_app/styles/theme.dart' as Style;
import 'package:toast/toast.dart';

class Streams extends StatefulWidget {
  final int id;

  Streams({Key key, @required this.id}) : super(key: key);

  @override
  _StreamState createState() => _StreamState(id);
}

class _StreamState extends State<Streams> {
  final int id;

  _StreamState(this.id);

  @override
  void initState() {
    super.initState();
    movieStreamsBloc..getMovieStream(id);
  }

  @override
  void dispose() {
    super.dispose();
    movieStreamsBloc..drainStream();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<StreamResponse>(
      stream: movieStreamsBloc.subject.stream,
      builder: (context, AsyncSnapshot<StreamResponse> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.error != null && snapshot.data.error.length > 0) {
            return _buildErrorWidget(snapshot.data.error);
          }
          return _buildMovieStreamsWidget(snapshot.data);
        } else if (snapshot.hasError) {
          return _buildErrorWidget(snapshot.error);
        } else {
          return _buildLoadingWidget();
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Container();
    // return Center(
    //   child: Column(
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     children: <Widget>[
    //       Text(
    //         "$error",
    //         style: TextStyle(color: Colors.white),
    //       ),
    //     ],
    //   ),
    // );
  }

  Widget _buildMovieStreamsWidget(StreamResponse data) {
    List<Movies> movies = data.streams.movies;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 10.0, top: 20.0),
          child: Text(
            "MOVIES",
            style: TextStyle(
              color: Style.Colors.titleColor,
              fontWeight: FontWeight.w500,
              fontSize: 12.0,
            ),
          ),
        ),
        Container(
          height: 65.0,
          padding: EdgeInsets.only(left: 10.0),
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: movies != null ? movies.length : 0,
            itemBuilder: (context, index) {
              return Container(
                padding: EdgeInsets.only(top: 10.0, right: 8.0),
                width: 220.0,
                child: ListTile(
                  leading: Icon(
                    EvaIcons.playCircleOutline,
                    color: Style.Colors.secondColor,
                  ),
                  title: Text(
                    movies[index].name != null ? movies[index].name : '',
                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                  ),
                  onTap: () {
                    if (movies[index].url != null) {
                      if(movies[index].select == "iframe") { // condition will be applied iframe
                        // iframe player
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => IFramePlayer(
                              url: movies[index].url,
                            ),
                          ),
                        );
                      } else {
                        // yoyo player
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HlsPlayer(
                              url: movies[index].url,
                            ),
                          ),
                        );
                      }
                    } else {
                      showToast("No video added!", duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                    }
                  },
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }
}
