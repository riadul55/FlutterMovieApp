import 'package:flutter/material.dart';
import 'package:movie_app/bloc/get_movie_img_bloc.dart';
import 'package:movie_app/models/details/imagense_response.dart';
import 'package:movie_app/repository/movie_repository.dart';
import 'package:movie_app/styles/theme.dart' as Style;

class Imagense extends StatefulWidget {
  final int id;

  Imagense({Key key, @required this.id}) : super(key: key);

  @override
  _ImagenseState createState() => _ImagenseState(id);
}

class _ImagenseState extends State<Imagense> {
  final int id;

  _ImagenseState(this.id);

  @override
  void initState() {
    super.initState();
    movieImagenseBloc..getMovieImagense(id);
  }

  @override
  void dispose() {
    super.dispose();
    // movieImagenseBloc..drainStream();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 10.0, top: 20.0),
          child: Text(
            "IMAGENSE",
            style: TextStyle(
              color: Style.Colors.titleColor,
              fontWeight: FontWeight.w500,
              fontSize: 12.0,
            ),
          ),
        ),
        SizedBox(
          height: 5.0,
        ),
        StreamBuilder<ImagenseResponse>(
          stream: movieImagenseBloc.subject.stream,
          builder: (context, AsyncSnapshot<ImagenseResponse> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.error != null &&
                  snapshot.data.error.length > 0) {
                return _buildErrorWidget(snapshot.data.error);
              }
              return _buildMoviesImgsWidget(snapshot.data);
            } else if (snapshot.hasError) {
              return _buildErrorWidget(snapshot.error);
            } else {
              return _buildLoadingWidget();
            }
          },
        ),
      ],
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "$error",
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }

  Widget _buildMoviesImgsWidget(ImagenseResponse response) {
    List<String> images = response.galleryImages;
    print(images);
    return Container(
      height: 200.0,
      padding: EdgeInsets.only(left: 10.0),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: images != null ? images.length : 0,
        itemBuilder: (context, index) {
          return Container(
            padding: EdgeInsets.only(top: 10.0, right: 8.0),
            width: 100.0,
            child: GestureDetector(
              onTap: () {},
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  images[index] != null ? Container(
                    width: 100,
                    height: 150,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              MovieRepository.tmdbImgOriginalUrl +
                                  images[index]),
                        )),
                  ) : Container(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
