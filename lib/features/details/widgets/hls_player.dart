import 'package:chewie/chewie.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:movie_app/models/details/episode_response.dart';
import 'package:video_player/video_player.dart';
import 'package:yoyo_player/yoyo_player.dart';

class HlsPlayer extends StatefulWidget {
  final String url;

  HlsPlayer({Key key, @required this.url}) : super(key: key);

  @override
  _HlsPlayerState createState() => _HlsPlayerState(url);
}

class _HlsPlayerState extends State<HlsPlayer> {
  final String url;

  _HlsPlayerState(this.url);

  var videoPlayerController;
  var chewieController;

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    super.initState();
    videoPlayerController = VideoPlayerController.network(this.url);
    videoPlayerController.initialize();

    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      autoPlay: true,
      looping: true,
      fullScreenByDefault: true,
      allowFullScreen: false,
    );
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    chewieController.dispose();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.black,
          body: Stack(
            children: <Widget>[
              Center(
                child: Chewie(
                  controller: chewieController,
                ),
              ),
              // Positioned(
              //   top: 40.0,
              //   right: 20.0,
              //   child: IconButton(
              //     icon: Icon(EvaIcons.closeCircle),
              //     color: Colors.white,
              //     onPressed: () {
              //       Navigator.pop(context);
              //     },
              //   ),
              // ),
            ],
          ),
        ),
        onWillPop: () {
          Navigator.pop(context);
          return;
        }
    );
  }
}
