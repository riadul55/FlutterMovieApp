import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/bloc/get_episodes_bloc.dart';
import 'package:movie_app/features/details/widgets/hls_player.dart';
import 'package:movie_app/models/details/episode_response.dart';
import 'package:movie_app/repository/movie_repository.dart';
import 'package:movie_app/styles/theme.dart' as Style;
import 'package:toast/toast.dart';

import 'iframe_player.dart';

typedef void HeightCallback(val);

class SeasonsEpisodes extends StatefulWidget {
  final String ids;
  final String temp;
  final HeightCallback callback;

  SeasonsEpisodes({Key key, @required this.ids, @required this.temp, this.callback})
      : super(key: key);

  @override
  _SeasonsEpisodesState createState() => _SeasonsEpisodesState(ids, temp);
}

class _SeasonsEpisodesState extends State<SeasonsEpisodes> {
  final String ids;
  final String temp;

  _SeasonsEpisodesState(this.ids, this.temp);

  @override
  void initState() {
    super.initState();
    episodesBloc..getEpisodesBySeason(ids, temp);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<EpisodeResponse>(
      stream: episodesBloc.subject.stream,
      builder: (context, AsyncSnapshot<EpisodeResponse> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.errors != null && snapshot.data.errors.length > 0) {
            return _buildErrorWidget(snapshot.data.errors);
          }
          return _buildEpisodsBySesonsWidget(snapshot.data);
        } else if (snapshot.hasError) {
          return _buildErrorWidget(snapshot.error);
        } else {
          return _buildLoadingWidget();
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("$error", style: TextStyle(color: Colors.white),),
        ],
      ),
    );
  }

  Widget _buildEpisodsBySesonsWidget(EpisodeResponse data) {
    List<Episode> episodes = data.episodes;
    if (episodes.length == 0) {
      return Container(
        child: Text("No Episodes"),
      );
    } else
      Future.microtask(() {
        setState(() {
          widget.callback(episodes.length);
        });
      });
      return Container(
        child: ListView.builder(
            itemCount: episodes.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.only(
                  top: 10.0,
                  bottom: 10.0,
                  right: 10.0,
                ),
                child: GestureDetector(
                  onTap: () {
                    Episode episode = episodes[index];
                    if (episode != null && episode.metas.players != null) {
                      if(episode.metas.players[0].select == "iframe") { // condition will be applied iframe
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => IFramePlayer(
                              url: episode.metas.players[0].url,
                            ),
                          ),
                        );
                      } else {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HlsPlayer(
                              url: episode.metas.players[0].url,
                            ),
                          ),
                        );
                      }
                    } else {
                      // show toast
                      showToast("No video added!", duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                    }
                  },
                  child: Card(
                    elevation: 4,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: episodes[index].metas.dtBackdrop == null
                              ? Container(
                            width: 50.0,
                            height: 130.0,
                            decoration: BoxDecoration(
                                color: Style.Colors.secondColor,
                                borderRadius:
                                BorderRadius.all(Radius.circular(2.0)),
                                shape: BoxShape.rectangle),
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  EvaIcons.filmOutline,
                                  color: Colors.white,
                                  size: 40.0,
                                ),
                              ],
                            ),
                          )
                              : Container(
                            width: 50.0,
                            height: 130.0,
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(2.0)),
                              shape: BoxShape.rectangle,
                              image: DecorationImage(
                                  image: NetworkImage(
                                      MovieRepository.tmdbImgOriginalUrl +
                                          episodes[index].metas.dtBackdrop),
                                  fit: BoxFit.cover),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  episodes[index].title,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0),
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      episodes[index].date,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 10.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      width: 5.0,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }),
      );
  }
  
  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }
}
