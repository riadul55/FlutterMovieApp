import 'package:flutter/material.dart';
import 'package:movie_app/bloc/get_seasons_bloc.dart';
import 'package:movie_app/features/details/widgets/season_list.dart';
import 'package:movie_app/models/details/season_response.dart';

class Seasons extends StatefulWidget {
  final String ids;

  Seasons({Key key, @required this.ids}) : super(key: key);

  @override
  _SeasonsState createState() => _SeasonsState(ids);
}

class _SeasonsState extends State<Seasons> {
  final String ids;

  _SeasonsState(this.ids);

  @override
  void initState() {
    super.initState();
    seasonsBloc..getMovieSeasons(ids);
  }

  @override
  void dispose() {
    super.dispose();
    seasonsBloc..drainStream();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<SeasonResponse>(
      stream: seasonsBloc.subject.stream,
      builder: (context, AsyncSnapshot<SeasonResponse> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.errors != null && snapshot.data.errors.length > 0) {
            return _buildErrorWidget(snapshot.data.errors);
          }
          return _buildGeneresWidget(snapshot.data);
        } else if (snapshot.hasError) {
          return _buildErrorWidget(snapshot.error);
        } else {
          return _buildLoadingWidget();
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 25.0,
            width: 25.0,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 4.0,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Container();
    // return Center(
    //   child: Column(
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     children: <Widget>[
    //       Text("$error"),
    //     ],
    //   ),
    // );
  }

  Widget _buildGeneresWidget(SeasonResponse data) {
    List<Season> seasons = data.seasons;
    if (seasons == null || seasons.length == 0) {
      return Container();
    } else
      return SeasonList(
        seasons: seasons,
      );
  }
}
