import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/features/search/search.dart';
import 'package:movie_app/styles/theme.dart' as Style;
import 'package:toast/toast.dart';

import 'widgets/geners.dart';
import 'widgets/now_playing.dart';
import 'widgets/recent_movies.dart';
import 'widgets/top_movies.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      body: ListView(
        children: <Widget>[
          NowPlaying(),
          Generes(),
          TopMovies(),
          RecentMovies()
        ],
      ),
    );
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }
}
