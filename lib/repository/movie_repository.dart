import 'package:dio/dio.dart';
import 'package:movie_app/models/details/cast_response.dart';
import 'package:movie_app/models/details/episode_response.dart';
import 'package:movie_app/models/details/imagense_response.dart';
import 'package:movie_app/models/details/info_response.dart';
import 'package:movie_app/models/details/season_response.dart';
import 'package:movie_app/models/details/stream_response.dart';
import 'package:movie_app/models/genre_response.dart';
import 'package:movie_app/models/movie_response.dart';

class MovieRepository {
  final String apiKey = "";
  static String mainUrl = "http://certayncrew.com/movie/wp-json";
  static String tmdbImgUrl = "https://image.tmdb.org/t/p/w92/";
  static String tmdbImgOriginalUrl = "https://image.tmdb.org/t/p/original/";

  final Dio _dio = Dio();

  var getFeaturedUrl = "$mainUrl/wc/v3/featured";
  var getRecentUrl = "$mainUrl/wc/v3/recent";
  var getPopularUrl = "$mainUrl/wc/v3/popular";
  var getMoviesUrl = "$mainUrl/wc/v3/movies";
  var getGenerUrl = "$mainUrl/wc/v3/genres";
  var getMovieByGenerUrl = "$mainUrl/wc/v3/byGenres/";
  var getRelatedUrl = "$mainUrl/wc/v3/related/";
  var getPersonUrl = "";

  //movie details
  var getMovieInfoUrl = "$mainUrl/wc/v3/details/info/";
  var getMovieCastUrl = "$mainUrl/wc/v3/details/casts/";
  var getMovieImagenseUrl = "$mainUrl/wc/v3/details/imagense/";
  //details relatables
  var getMovieStreamsUrl = "$mainUrl/wc/v3/details/streams/"; //with post id
  var getTvShowsSeasonsUrl = "$mainUrl/wc/v3/details/seasons/"; //with post ids
  var getTvShowsEpisodesUrl =
      "$mainUrl/wc/v3/details/episodes/"; //with post ids

  //queries
  var getMoviesByQueryUrl = "$mainUrl/wc/v3/all/search";

  Future<MovieResponse> getMovies() async {
    try {
      Response response = await _dio.get(getMoviesUrl);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieResponse> getPlaying() async {
    try {
      Response response = await _dio.get(getFeaturedUrl);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieResponse> getPopular() async {
    try {
      Response response = await _dio.get(getPopularUrl);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieResponse> getRecent() async {
    try {
      Response response = await _dio.get(getRecentUrl);
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return MovieResponse.withError("$error");
    }
  }

  Future<GenreResponse> getGenres() async {
    try {
      Response response = await _dio.get(getGenerUrl);
      return GenreResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return GenreResponse.withError("$error");
    }
  }

  Future<MovieResponse> getMovieByGenres(int id) async {
    try {
      Response response = await _dio.get("$getMovieByGenerUrl$id");
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieResponse> getRelatedMovies(int id) async {
    try {
      Response response = await _dio.get("$getRelatedUrl$id");
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return MovieResponse.withError("$error");
    }
  }

  //details
  Future<InfoResponse> getMovieInfo(int id) async {
    try {
      Response response = await _dio.get("$getMovieInfoUrl$id");
      return InfoResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return InfoResponse.withError("$error");
    }
  }

  Future<CastResponse> getMovieCasts(int id) async {
    try {
      Response response = await _dio.get("$getMovieCastUrl$id");
      return CastResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return CastResponse.withError("$error");
    }
  }

  Future<ImagenseResponse> getMovieImagense(int id) async {
    try {
      Response response = await _dio.get("$getMovieImagenseUrl$id");
      return ImagenseResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return ImagenseResponse.withError("$error");
    }
  }

  //get more details
  Future<StreamResponse> getMovieStreams(int id) async {
    try {
      Response response = await _dio.get("$getMovieStreamsUrl$id");
      return StreamResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return StreamResponse.withError("$error");
    }
  }

  Future<SeasonResponse> getTvShowsSeasons(String ids) async {
    try {
      Response response = await _dio.get("$getTvShowsSeasonsUrl$ids");
      return SeasonResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return SeasonResponse.withError("$error");
    }
  }

  Future<EpisodeResponse> getTvShowsEpisodes(String ids, String temp) async {
    try {
      Response response = await _dio.get("$getTvShowsEpisodesUrl$ids/$temp");
      return EpisodeResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return EpisodeResponse.withError("$error");
    }
  }

  // get movies by query
  Future<MovieResponse> getMoviesByQuery(String query) async {
    try {
      Response response = await _dio
          .get("$getMoviesByQueryUrl", queryParameters: {"query": query});
      return MovieResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      return MovieResponse.withError("$error");
    }
  }
}
