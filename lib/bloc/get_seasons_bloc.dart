import 'package:flutter/material.dart';
import 'package:movie_app/models/details/season_response.dart';
import 'package:movie_app/repository/movie_repository.dart';
import 'package:rxdart/rxdart.dart';

class SeasonsBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<SeasonResponse> _subject =
      BehaviorSubject<SeasonResponse>();

  getMovieSeasons(String ids) async {
    SeasonResponse response = await _repository.getTvShowsSeasons(ids);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<SeasonResponse> get subject => _subject;
}

final seasonsBloc = SeasonsBloc();
