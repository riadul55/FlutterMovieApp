import 'package:flutter/material.dart';
import 'package:movie_app/models/details/imagense_response.dart';
import 'package:movie_app/repository/movie_repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieImagenseBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<ImagenseResponse> _subject =
      BehaviorSubject<ImagenseResponse>();

  getMovieImagense(int id) async {
    ImagenseResponse response = await _repository.getMovieImagense(id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<ImagenseResponse> get subject => _subject;
}

final movieImagenseBloc = MovieImagenseBloc();
