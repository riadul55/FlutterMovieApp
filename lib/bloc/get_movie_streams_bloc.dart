import 'package:flutter/material.dart';
import 'package:movie_app/models/details/stream_response.dart';
import 'package:movie_app/repository/movie_repository.dart';
import 'package:rxdart/rxdart.dart';

class MoviesStreamsBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<StreamResponse> _subject =
      BehaviorSubject<StreamResponse>();

  getMovieStream(int id) async {
    StreamResponse response = await _repository.getMovieStreams(id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<StreamResponse> get subject => _subject;
}

final movieStreamsBloc = MoviesStreamsBloc();
