import 'package:flutter/material.dart';
import 'package:movie_app/models/details/info_response.dart';
import 'package:movie_app/repository/movie_repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieInfoBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<InfoResponse> _subject =
      BehaviorSubject<InfoResponse>();

  getMovieInfo(int id) async {
    InfoResponse response = await _repository.getMovieInfo(id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<InfoResponse> get subject => _subject;
}

final movieInfoBloc = MovieInfoBloc();
