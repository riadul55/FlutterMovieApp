import 'package:flutter/cupertino.dart';
import 'package:movie_app/models/details/episode_response.dart';
import 'package:movie_app/repository/movie_repository.dart';
import 'package:rxdart/rxdart.dart';

class EpisodesBloc {
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<EpisodeResponse> _subject =
      BehaviorSubject<EpisodeResponse>();

  getEpisodesBySeason(String ids, String temp) async {
    print("getTvShows ===> Id = $ids, temp = $temp");
    EpisodeResponse response = await _repository.getTvShowsEpisodes(ids, temp);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<EpisodeResponse> get subject => _subject;
}

final episodesBloc = EpisodesBloc();
